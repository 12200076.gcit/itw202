import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>


    // <>
    // <View style={styles.style1} />
    // <View style={styles.style2} />
    // </>

    // <View style={styles.container}>
    //   <View style={styles.square} />
    //   <View style={styles.square} />
    //   <View style={styles.square} />
    // </View>

    // <View style={styles.container}>
    //   <View style={styles.item} />
    //   <View style={styles.item} />
    //   <View style={styles.item} />
    // </View>
    <View style={styles.container}>
      <View style={styles.square} />
      <View style={styles.square} />
      <View style={styles.square} />
    </View>
    
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: 'lightgoldenrodyellow',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });

// const styles = StyleSheet.create({ style1: {
//     flex: 1,
//     backgroundColor: 'lightgoldenrodyellow',
//     alignItems: 'center',
//     justifyContent: 'center',
//   }, style2: {
//     flex: 3,
//     backgroundColor: '#7CA1B4',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   });

  // const styles = StyleSheet.create({
  //   container: {
  //     flex: 1,
  //     backgroundColor: '#7CA1B4',
  //     alignItems: 'center',
  //     justifyContent: 'center',
  //     // flexDirection: "column"
  //     flexDirection: "row"
  //   },
  //   square: {
  //     backgroundColor: "#7cb48f",
  //     width: 100,
  //     height: 100,
  //     margin: 4,
  //   },
  // });

  // const styles = StyleSheet.create({
  //   container: {
  //     flex: 1,
  //     backgroundColor: '#7CA1B4',
  //     alignItems: 'flex-start',
  //     justifyContent: 'flex-start',
  //     // flexDirection: "column"
  //     flexDirection: "column"
  //   },
  //   item: {
  //     backgroundColor: "lightgoldenrodyellow",
  //     width: 150,
  //     height: 150,
  //     borderColor: 'goldenrod',
  //     borderWidth: 1
  //   },
  // });

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#7CA1B4',
      // alignItems: 'center',
      alignItems: 'flex-end',
      justifyContent: 'center',
      
    },
    square: {
      backgroundColor: "#7cb48f",
      width: 100,
      height: 100,
      margin: 4,
    },
  });

