import React, {useState} from 'react';
import { StatusBar } from 'expo-status-bar';
import { Button, FlatList, StyleSheet, Text,TextInput, View } from 'react-native';
import { AspirationItem } from './components/AspirationItem';
import Aspirationinput from './components/CoreComponents';

export default function App() {
  const [courseAspirations, setCourseAspirations] = useState([]); 
  const [isAddMode, setIsAddMode] = useState(false);
  

  const addAspirationHandler = aspirationTitle =>{
    setCourseAspirations(currentAspirations => [
      ...currentAspirations,
      {key: Math.random().toString(), value: aspirationTitle }
    ])
    setIsAddMode(false)
  };

  const removeAspirationHandler = aspirationKey => {
    setCourseAspirations(currentAspirations => {
      return currentAspirations.filter((aspiration)=> aspiration.key !== aspirationKey)
    })
  }
    return (
    <View style={styles.screen}>
      <Button title=' Add New Aspiration' onPress={() => setIsAddMode(true)} />
      <Aspirationinput visible= {isAddMode} onAddAspiration = {addAspirationHandler} />
      <FlatList
        data={courseAspirations}
        renderItem = {itemdata =>
        <AspirationItem id={itemdata.item.key} onDelete={removeAspirationHandler} title ={itemdata.item.value} />
      } />
      <StatusBar style="auto" />
        
    </View>
  );
}

const styles = StyleSheet.create({
  screen:{
    padding: 50,
    },
    inputContainer:{
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems:'center'
    },
    input:{
      width:'80%',
      borderColor: 'black',
      borderWidth: 1,
      padding: 10
    }
});

// import { StatusBar } from 'expo-status-bar';
// import { Button, ProgressViewIOSComponent, StyleSheet, Text, TextInput, View, Modal } from 'react-native';

// export default function App() {
//   return (
//     <Modal visible= {props.visible} animationType="slide">
//     <View style={styles.container}>
//       <TextInput
//       placeholder="My Aspiration from this module"
//       style={styles.input}
//       onChangeText={AspirationInputHandler}
//       value={enteredAspiration}
//       />
//       <Button
//       tittle='ADD'
//       onPress={() => props.onAddAspiration(enteredAspiration)}
//       />
//       <Button
//       tittle='CANCEL'
//       onPress={() => props.onAddAspiration(enteredAspiration)}
//       />
//     </View>
//     </Modal>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   input: {
//     width: '80%',
//     borderColor: 'black',
//     borderWidth: 1,
//     padding: 10,
//     marginBottom:10
//   }
// });

// const onAddAspirationHandler = aspirationTitle => {
  
// }
