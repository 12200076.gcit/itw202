import { firebase } from '@firebase/app'

const CONFIG = {
  apiKey: "AIzaSyC23Bxe5Dhq1ioMI_MRaD3pU7Xb7fKbGUI",
  authDomain: "sanga-f2505.firebaseapp.com",
  databaseURL: "https://sanga-f2505-default-rtdb.firebaseio.com",
  projectId: "sanga-f2505",
  storageBucket: "sanga-f2505.appspot.com",
  messagingSenderId: "528874897395",
  appId: "1:528874897395:web:53947c933baa9ead3e5c86"
};
firebase.initializeApp(CONFIG)

export default firebase;
