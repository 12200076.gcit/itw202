import React from "react";
import { StyleSheet, Text, View  } from "react-native";


const MyComponent = () => {
    const first = 'Getting started with react native!';
    const second = 'Sonam Wangmo'
    return (
        <View style={styles.textStyle}>
            <Text style = {styles.textStyle1}>{first}</Text>
            <Text style = {styles.textStyle2}>My name is {second} </Text>
        </View>
    )
};
export default MyComponent; 
const styles = StyleSheet.create({
    textStyle: {
        fontSize: 24,
        flex: 1,
        textAlign: 'left',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStyle1: {
        fontSize: 45,
        textAlign: 'left',
    },
    textStyle2: {
        fontSize: 20
    },
});
