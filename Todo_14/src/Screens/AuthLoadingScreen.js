import React from "react";
import { ActivityIndicator } from "react-native-paper";
import Background from "../components/Background";
import firebase from "firebase";

export default function AuthLoadingScreen({navigation}){
    firebase.auth().onAuthStateChanged((user) => {
        if (user) {
            navigation.reset({
                routes: [{name: "HomeScreen"}],
            });
        } else {
            navigation.reset({
                routes: [{ name: "StarScreen"}],
            });
        }
    });
    return (
        <Background>
            <ActivityIndicator size="large"/>
        </Background>    
    )
}