import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Todo11Example from './components/CoreComponent';
import TouchableHighlightE from './components/TouchableHeightlight';

export default function App() {
  return (
    <View style= {styles.container}>
      <Todo11Example></Todo11Example>
      <TouchableHighlightE></TouchableHighlightE>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-end',
    marginBottom:100 ,
    
  },
});






