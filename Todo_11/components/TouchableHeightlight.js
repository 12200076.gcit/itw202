import React, { useState } from 'react'
import {View, Text, StyleSheet, Button, TouchableWithoutFeedback, TouchableHighlight } from 'react-native'

const TouchableHighlightE = () => {
    const [count, setCount] = useState(0);
return (
    <View style ={{padding:100}}>
        <Text style={{lineHeight:50}}> The button isn't pressed yet {count} times! </Text>
        <TouchableHighlight style={styles.button}
            onPress={() => setCount(count + 1)}>
            <Text>PRESS ME</Text>
        </TouchableHighlight>
    </View>
)    
}
const styles= StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10
    }
})
export default TouchableHighlightE;