import React, { useState } from "react";
import { StyleSheet, Text, View, TextInput } from "react-native";
// import styles from "../../Todo8/components/MyStyles";

export default function Todo10() {
    const [text, setText] = useState('');

        return (
            <View style={styles.container}>
           
                <Text> What is your name? </Text>
                <TextInput secureTextentry style = {{borderWidth:1, width:100}}
                 onChangeText={text=>setText(text)}
                 secureTextEntry={true}/>
                 <Text>Hi {text} from gyelpozhing college of information of technologies </Text>
            </View>
        )
}
const styles=StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'center',
        alignItems: 'center',
    }
})