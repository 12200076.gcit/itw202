import React from "react";
import {View, Image, StyleSheet} from 'react-native';

const ImageComponent = () => {
    return(
        <View style= {styles.Contain} >
             <Image
            style= {styles.logoContain}
            source={{uri: 'https://picsum.photos/100/100'}}
            />
            <Image
            style= {styles.logoContain}
            source={require('../assets/react-native.png')}
            />
            
        </View>
    )
}
export default ImageComponent

const styles = StyleSheet.create({
    Contain: {
        backgroundColor:"#fff",
        flex:1,
        justifyContent:"center",
        alignItems:"center",
       
        
    },
    logoContain: {
        width: 100, height: 100,
       
        
    }
});
