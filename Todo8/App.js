import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import styles from './components/MyStyles';

export default function App() {
  const t1 = <Text style = {styles.innertext}> quick brown fox</Text>
  return (
    <View style={styles.container}>
      <Text style= {styles.wholetext}>The {t1} jumps over the lazy dog</Text>
      <StatusBar style="auto" />
    </View>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
