import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Courses from './components/courses';
import CountClass from './components/countClass';
import CountHook from './components/CountHook';

export default function App() {
  return (
    <View style={styles.container}>
      <Courses></Courses>
      <CountClass/>
      <CountHook/>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
